import React from 'react'
import AddTodo from '../containers/AddTodo'
import VisibleTodoList from '../containers/VisibleTodoList'
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Footer from '../containers/footer'

const App = () => (
  <div style={{backgroundColor:"#696868"}}>
    <Container maxWidth="md" style={{ marginTop: '20px', minHeight:'60vh', }}>
      <Box boxShadow={1} style={{ textAlign: 'center', padding: '10px', color: '#555' ,backgroundColor:"#a8db7e"}}>
        <Typography variant="h4" gutterBottom fontWeight="fontWeightBold">
            <b>Task Manager</b>
            <Box p={1} />
        </Typography>
        <AddTodo />
        <Box p={1} />
        <VisibleTodoList />
        <Footer/>
      </Box>
    </Container>
    
  </div>
)

export default App